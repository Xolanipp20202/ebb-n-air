import { Component, OnInit,  ViewChild, ElementRef, NgZone, ɵConsole  } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { FormBuilder, FormGroup } from '@angular/forms';
import { google } from "google-maps";

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  @ViewChild('map', { static: false }) mapElement: ElementRef;
  map: any;
  GoogleAutocomplete: google.maps.places.AutocompleteService;
  Geocoder: google.maps.Geocoder;
  autocomplete: { input: string; };
  autocompleteItems: any[];
  location: any;
  placeid: any;
  latitude: any;
  longitude: any;
  public Form:FormGroup;

  constructor(
    public formBuilder: FormBuilder,
    public zone: NgZone,
    private geolocation: Geolocation) {
    this.Form = formBuilder.group({
        name: [''],
        city: [''],
        address: ['']
    });
    this.GoogleAutocomplete = new google.maps.places.AutocompleteService();
    this.Geocoder = new  google.maps.Geocoder
    this.autocomplete = { input: '' };
    this.autocompleteItems = [];
  }

 

  ngOnInit() {
    this.drawMap(null);
  }

  // create/update map initialising with current location
  drawMap(latLng) {
    if(!latLng){
      this.geolocation.getCurrentPosition().then((resp) => {

        this.latitude = resp.coords.latitude;
        this.longitude = resp.coords.longitude;

        let latLng = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);
        let mapOptions = {
          center: latLng,
          zoom: 15,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }

        this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
  
      }).catch((error) => {
        console.log('Error getting location', error);
      });
    }
    else{
      let mapOptions = {
        center: latLng,
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      }
      this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    }
  }
  
  updateSearchText(){

    if (this.autocomplete.input == '') {
      this.autocompleteItems = [];
      return;
    }
    this.GoogleAutocomplete.getPlacePredictions({ input: this.autocomplete.input },
    (predictions, status) => {
      this.autocompleteItems = [];
      this.zone.run(() => {
        predictions.forEach((prediction) => {
          this.autocompleteItems.push(prediction);
        });
      });
    });
  }

  selectSearchText(item) {
    this.location = item
    this.placeid = this.location.place_id
    this.Form.controls['address'].setValue(this.location.description);
    this.getCoordsFromPlace(this.placeid)
    setTimeout( () => {
      this.autocompleteItems = [];
   },700);
  }

  getCoordsFromPlace(placeId){
    var place = {
      placeId:placeId,
    }
    this.Geocoder.geocode(place,
      (results,status) => {
        if (status == google.maps.GeocoderStatus.OK){
        var location = results[0].geometry.location
        this.latitude = location.lat();
        this.longitude = location.lng();
        console.log()
        this.drawMap(location);
      }
    })
  }

  onSubmit() {
    console.log("Name: ",this.Form.get("name").value)
    console.log("City: ",this.Form.get("city").value)
    console.log("Latitude: ",this.latitude)
    console.log("Longitude: ",this.longitude)
    console.log("String Address: ",this.Form.get("address").value)
  }


}